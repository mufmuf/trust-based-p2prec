import pandas as pd
import json
import os
from sklearn.model_selection import StratifiedKFold
import sys

directory = sys.argv[1]
file_prefix = directory.split('/')[1]
os.makedirs(f'{directory}/ratings-folds', exist_ok=True)
with open(f'{directory}/{file_prefix}-ratings.jl') as f:
    records = [json.loads(line) for line in f]
ratings = pd.DataFrame.from_records(records).dropna(subset=['rating']) #.sort_values(by='updated_at')
ratings = ratings[~ratings.duplicated(['username', 'film_id'], keep='last')]
with open(f'{directory}/{file_prefix}-users.jl') as f:
    test_users = [json.loads(line).get('username') for line in f]
mask = ratings.username.isin(test_users)
test_ratings = ratings[mask]
train_ratings = ratings[~mask]
X = test_ratings.drop('username', axis=1)
y = test_ratings.username
skf = StratifiedKFold(n_splits=5, shuffle=True)
for i, (train_idx, test_idx) in enumerate(skf.split(X, y)):
    train = test_ratings.iloc[train_idx]
    test = test_ratings.iloc[test_idx]

    pd.concat([train, train_ratings]).to_json(f'{directory}/ratings-folds/train-{i}.jl', orient='records', lines=True)
    test.to_json(f'{directory}/ratings-folds/test-{i}.jl', orient='records', lines=True)
