import { ItemID, UserID } from './types'

export const evaluate = (recommendedRanking: readonly ItemID[], relevantItems: Set<ItemID>, topK: number): Evaluation => {
  if (relevantItems.size === 0) {
    throw new Error('There should be at least one relevant item ' +
      'to be able to evaluate the recommender meaningfully.')
  }
  const scoredRanking = recommendedRanking.map(scoreItem(relevantItems)).slice(0, topK)
  const hitLength = getHitLength(scoredRanking)
  const precision = getPrecision(hitLength, topK)
  const recall = getRecall(hitLength, relevantItems)
  return {
    precision,
    recall,
    f1: 2 * precision * recall / (precision + recall + 0.000000001), // so that f1 is never NaN
    ndcg: getNDCG(scoredRanking, relevantItems, topK)
  }
}

const scoreItem = (relevantItems: Set<ItemID>) => (item: ItemID): ItemScoring => {
  if (relevantItems.has(item)) {
    return { item, score: 1 }
  } else {
    return { item, score: 0 }
  }
}

const getHitLength = (scoredRanking: readonly ItemScoring[]): number => {
  return scoredRanking.reduce((total, { score }) => total + score, 0)
}

const getPrecision = (hitLength: number, topK: number): number => {
  return hitLength / topK
}

const getRecall = (hitLength: number, relevantItems: Set<ItemID>): number => {
  return hitLength / relevantItems.size
}

const discountedGain = ({ score }: Pick<ItemScoring, 'score'>, idx: number): number => {
  return (Math.pow(2, score) - 1) / Math.log2(idx + 2)
}

const getDCG = (scoredRanking: readonly ItemScoring[]): number => {
  return scoredRanking.map(discountedGain).reduce((a, b) => a + b, 0)
}

const getIDCG = (relevantItems: Set<ItemID>, topK: number): number => {
  let idcg = 0
  for (let i = 0; i < Math.min(relevantItems.size, topK); i++) {
    idcg += discountedGain({ score: 1 }, i)
  }
  return idcg
}

const getNDCG = (scoredRanking: ItemScoring[], relevantItems: Set<ItemID>, topK: number): number => {
  return getDCG(scoredRanking) / getIDCG(relevantItems, topK)
}

interface ItemScoring {
  item: ItemID
  score: number
}

export interface Evaluation {
  precision: number
  recall: number
  f1: number
  ndcg: number
}

export interface ExperimentalResults {
  user: UserID
  recommendations: readonly ItemID[]
  testItems: Set<ItemID>
  relevantItems: Set<ItemID>
  neighbours: ItemID[]
  evaluation: Evaluation
}
