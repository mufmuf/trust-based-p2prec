import { UserID, SocialNetwork, UserWeight } from '../types'
import { computeDistances, getAllValues, intersection, takeKeyOfEntry } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class EgonRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'egon'
    })
  }

  async computeTrustScores (user: UserID, { followees, followers }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')

    const users = getAllValues(followees)
    const userFollowees = followees.get(user) as Set<UserID>
    const trustScores: UserWeight[] = []
    users.forEach(user2 => {
      const user2Followers = followers.get(user2) as Set<UserID>
      trustScores.push({ user: user2, weight: intersection(userFollowees, user2Followers).size / user2Followers.size })
    })
    // const distances = [...computeDistances(user, followees, depth).entries()]
    // const hop1Users = distances.filter(([_, distance]) => distance === 1).map(takeKeyOfEntry)
    // distances.map(takeKeyOfEntry).forEach(user2 => {
    //   trustScores.push({ user: user2, weight: intersection(followers.get(user2) as Set<UserID>, new Set(hop1Users)).size })
    // })

    return trustScores
  }
}
