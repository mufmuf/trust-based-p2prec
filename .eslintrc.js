module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'standard-with-typescript'
  ],
  parserOptions: {
    project: 'tsconfig.json'
  },
  rules: {
    '@typescript-eslint/method-signature-style': ['error', 'method']
  }
}
