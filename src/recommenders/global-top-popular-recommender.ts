import { BaseRecommender, IBaseRecommenderData, IBaseRecommenderOptions } from './base-recommender'
import { Ratings, UserID, ItemID, ItemPrediction } from '../types'

export class GlobalTopPopularRecommender extends BaseRecommender {
  itemPopularity: Ratings = new Map()

  constructor (opts: Omit<IBaseRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'global-top-popular'
    })
  }

  fit ({ userRatings }: IBaseRecommenderData): void {
    this.userRatings = userRatings
    for (const [, ratings] of this.userRatings) {
      for (const item of ratings.keys()) {
        const popularity = this.itemPopularity.get(item) ?? 0
        this.itemPopularity.set(item, popularity + 1)
      }
    }
  }

  async getPredictions (user: string, candidateItems: Set<string>): Promise<ItemPrediction[]> {
    return Array.from(this.itemPopularity)
      .filter(([item]) => candidateItems.has(item))
      .map(item => ({ item: item[0], prediction: item[1] }))
  }

  async recommend (user: UserID, candidateItems: Set<ItemID>, topK: number): Promise<ItemID[]> {
    const unsortedPredictions = await this.getPredictions(user, candidateItems)
    return this.selectTopKRecommendedItems(unsortedPredictions, topK)
  }
}
