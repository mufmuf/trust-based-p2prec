import { UserID, SocialNetwork, UserWeight, FollowIndex } from '../types'
import { getAllValues, intersection } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class ResourceAllocationRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'adamic-adar'
    })
  }

  async computeTrustScores (user: UserID, { followees, followers }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const users = getAllValues(followees)
    users.delete(user)
    const trustScores: UserWeight[] = []
    users.forEach(user2 => {
      trustScores.push({ user: user2, weight: this._resourceAllocationIndex(intersection(followees.get(user) as Set<UserID>, followers.get(user2) as Set<UserID>), followers) })
    })

    return trustScores
  }

  _resourceAllocationIndex (users: Set<UserID>, followers: FollowIndex): number {
    return [...users].map(user => (followers.get(user) as Set<UserID>).size).reduce((total, numFollowers) => total + 1 / numFollowers, 0)
  }
}
