import * as fs from 'fs'
import * as path from 'path'
import * as readline from 'readline'
import * as sqlite3 from 'sqlite3'
import { ExperimentalResults } from './evaluation'
import { Dataset, Entry, FollowIndex, ItemID, ItemPrediction, Ratings, RatingsIndex, TrustScores, UserID, UserWeight } from './types'

sqlite3.verbose()

const cwd = process.cwd()

export interface ReaderOpts {
  train: string
  test: string
  socialNetwork: string
  recommenderName: string
  useTrustCache: boolean
}

export const readDatasets = async ({ train, test, socialNetwork, recommenderName, useTrustCache }: ReaderOpts): Promise<Dataset> => {
  const followeeIndex: FollowIndex = new Map()
  const followerIndex: FollowIndex = new Map()
  const userTrainRatings: RatingsIndex = new Map()
  // TODO: itemRatings are not needed anymore. remove them.
  const itemTrainRatings: RatingsIndex = new Map()
  const testRatings: RatingsIndex = new Map()
  const trustScores: TrustScores = new Map()

  return await new Promise((resolve, reject) => {
    const rlNetwork = readline.createInterface({
      input: fs.createReadStream(path.join(cwd, socialNetwork)),
      output: process.stdout,
      terminal: false
    })

    rlNetwork.on('line', (line: string) => {
      const jsonRep = JSON.parse(line)
      // make sure to convert the numbers to string just in case
      const { username, follows } = { username: jsonRep.username.toString(), follows: jsonRep.follows.toString() }
      const followees = followeeIndex.get(username) ?? new Set()
      followees.add(follows)
      if (!followeeIndex.has(username)) followeeIndex.set(username, followees)
      const followers = followerIndex.get(follows) ?? new Set()
      followers.add(username)
      if (!followerIndex.has(follows)) followerIndex.set(follows, followers)
    }).on('close', function () {
      const rlTrainRatings = readline.createInterface({
        input: fs.createReadStream(path.join(cwd, train)),
        output: process.stdout,
        terminal: false
      })

      rlTrainRatings.on('line', line => {
        const ratingStatement = JSON.parse(line) as RatingStatement
        const userRatings = userTrainRatings.get(ratingStatement.username) ?? new Map()
        const itemRatings = itemTrainRatings.get(ratingStatement.film_id) ?? new Map()
        userRatings.set(ratingStatement.film_id, parseInt(ratingStatement.rating))
        itemRatings.set(ratingStatement.username, parseInt(ratingStatement.rating))
        if (!userTrainRatings.has(ratingStatement.username)) {
          userTrainRatings.set(ratingStatement.username, userRatings)
        }
        if (!itemTrainRatings.has(ratingStatement.film_id)) {
          itemTrainRatings.set(ratingStatement.film_id, itemRatings)
        }
      }).on('close', function () {
        const rlTestRatings = readline.createInterface({
          input: fs.createReadStream(path.join(cwd, test)),
          output: process.stdout,
          terminal: false
        })

        rlTestRatings.on('line', line => {
          const ratingStatement = JSON.parse(line) as RatingStatement
          const userRatings = testRatings.get(ratingStatement.username) ?? new Map()
          userRatings.set(ratingStatement.film_id, parseInt(ratingStatement.rating))
          if (!testRatings.has(ratingStatement.username)) {
            testRatings.set(ratingStatement.username, userRatings)
          }
        }).on('close', function () {
          const dataset: Dataset = {
            ratings: {
              trainItemRatings: itemTrainRatings,
              trainUserRatings: userTrainRatings,
              testRatings: testRatings
            },
            socialNetwork: {
              followees: followeeIndex,
              followers: followerIndex
            },
            trustScores: trustScores
          }

          if (!useTrustCache) {
            resolve(dataset)
            return
          }

          const db = new sqlite3.Database('./db/trust-scores.db')
          const sql = 'SELECT user_id, recommender, trust_scores FROM trust_scores ' +
            'WHERE recommender = ?;'

          db.all(sql, recommenderName, (err, rows) => {
            if (err instanceof Error) {
              reject(err.message)
              return
            }
            rows.forEach(({ user_id: userId, trust_scores: userTrustScores }: { user_id: string, trust_scores: string }) => {
              trustScores.set(userId, JSON.parse(userTrustScores) as UserWeight[])
            })
            resolve(dataset)
          })
          db.close()
        })
      })
    })
  })
}

export const computeDistances = (user: UserID, followingIndex: FollowIndex, depth: number): Map<UserID, number> => {
  const distances: Map<UserID, number> = new Map()
  if (!followingIndex.has(user)) return distances
  for (const neighbour of (followingIndex.get(user) as Set<UserID>)) {
    distances.set(neighbour, 1)
  }

  let reachedDepth = 1
  let reachedNodes = new Set(distances.keys())
  // REMARK: inefficient implementation. There are unnecessary loops for the sake of code readability.
  while (depth !== reachedDepth) {
    for (const reachedNode of reachedNodes) {
      if (!followingIndex.has(reachedNode)) continue
      for (const neighbour of (followingIndex.get(reachedNode) as Set<UserID>)) {
        if (!reachedNodes.has(neighbour) && neighbour !== user) {
          distances.set(neighbour, reachedDepth + 1)
        }
      }
    }
    reachedNodes = new Set(distances.keys())
    reachedDepth += 1
  }

  return distances
}

// cosine similarity using damping factor
export const computeCosineSimilarity = (ratings1: Ratings, ratings2: Ratings): number => {
  let numCommonItems = 0
  let sum = 0
  let ratings1norm = 0
  let ratings2norm = 0
  for (const [item1, rating1] of ratings1) {
    if (ratings2.has(item1)) {
      numCommonItems += 1
      const rating2 = (ratings2.get(item1) as number)
      sum += rating1 * rating2
      ratings1norm += rating1 * rating1
      ratings2norm += rating2 * rating2
    }
  }
  if (numCommonItems === 0) return 0
  // const dampingFactor = 1 / (1 + Math.exp(-numCommonItems / 4)) // logistic/sigmoid function
  // as an alternative the "damping term" mentioned here can be used as well (page 90, 93)
  // https://files.grouplens.org/papers/FnT%20CF%20Recsys%20Survey.pdf
  // const dampingFactor = min{|Iu ∩ Iv| / dampingCoef, 1}
  // where dampingCoef can be (10, 25, 50 etc.)
  const dampingFactor = Math.min(numCommonItems / 25, 1)

  // TODO:
  // this is magically working better. It doesn't limit the similarities between 0 and 1.
  // There is more nuance. But not giving stable results? For example, what is the relation between
  // users having more rated items in common and their similarity score?
  // const result = dampingFactor * sum / numCommonItems
  const result = dampingFactor * sum / (Math.sqrt(ratings1norm) * Math.sqrt(ratings2norm))

  return Math.max(result, 0) // this effectively removes the negatively correlated neighbours
}

export const takeKeyOfEntry = (entry: [string, any]): string => entry[0]

// applies the given function to the value of the entry.
// looks somehow very confusing in order to get the types right
export const apV = <K, V, S>(fn: (_: V) => S) => ([key, value]: [K, V]): [K, S] => [key, fn(value)]

// equivalent to this
// export function apV<K, V, S> (fn: (_: V) => S) {
//   return function ([key, value]: [K, V]): [K, S] {
//     return [key, fn(value)]
//   }
// }

// when scores of rankings are equal, sort them according to their usernames;
// so that every sorting is deterministic
export const sortRankings = (a: ItemPrediction, b: ItemPrediction): number => {
  const result = b.prediction - a.prediction
  if (result === 0) return a.item.toString().localeCompare(b.item.toString())
  return result
}

export const shuffle = (arr: any[]): void => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]]
  }
}

export const saveRecommendations = async (directory: string, filename: string, results: ExperimentalResults[]): Promise<void> => {
  fs.mkdir(directory, (err) => {
    if (err !== null && err.code !== 'EEXIST') return console.error(err)
    const recommendationsHeader = 'user,recommendations,testItems,relevantItems,neighbourhood\n'
    const recommendationsData = results.map(r => experimentalResultToCsvValues(r).join(',')).join('\n')
    fs.writeFile(path.join(directory, filename), recommendationsHeader + recommendationsData + '\n', () => {})
  })
}

const experimentalResultToCsvValues = (result: ExperimentalResults): string[] => {
  return [
    result.user,
    arrayToCsvValue(result.recommendations),
    arrayToCsvValue([...result.testItems]),
    arrayToCsvValue([...result.relevantItems]),
    arrayToCsvValue(result.neighbours)
  ]
}

const arrayToCsvValue = (arr: readonly string[]): string => {
  return '"[' + arr.join(',') + ']"'
}

export const takeRelevantTestItems = (train: Ratings, test: Ratings, maxRating: number): Set<ItemID> => {
  const trainSum = Array.from(train.values()).reduce((a: number, b: number) => a + b)
  const testSum = Array.from(test.values()).reduce((a: number, b: number) => a + b)
  const averageRating = Math.min(0.75 * maxRating, (trainSum + testSum) / (train.size + test.size))

  return new Set(Array.from(test).filter(([,rating]) => rating >= averageRating).map(takeKeyOfEntry))
}

export const sumValuesOfEntries = (entry1: Array<Entry<number>>, entry2: Array<Entry<number>>): Array<Entry<number>> => {
  return entry1.map(([k, v], idx) => [k, v + entry2[idx][1]])
}

export const intersection = <T>(set1: Set<T>, set2: Set<T>): Set<T> => {
  return new Set([...set1].filter(x => set2.has(x)))
}

export const union = <T>(set1: Set<T>, set2: Set<T>): Set<T> => {
  return new Set([...set1, ...set2])
}

export const takeMapSubset = <K, V>(m: Map<K, V>, keys: K[]): Map<K, V> => {
  return new Map(keys.map(k => [k, m.get(k)]).filter(([_, v]) => v !== undefined) as Array<[K, V]>)
}

export const getAllValues = <K, V>(m: Map<K, Set<V>>): Set<V> => {
  return [...m].reduce((allValues, [_, currSet]) => union(allValues, currSet), new Set<V>())
}

export const invertRatingsIndex = (ratingsIndex: RatingsIndex): RatingsIndex => {
  const invertedIndex = new Map<string, Ratings>()
  ratingsIndex.forEach((ratings, user) => {
    ratings.forEach((rating, item) => {
      if (invertedIndex.has(item)) {
        (invertedIndex.get(item) as Ratings).set(user, rating)
      } else {
        const newItemIndex = new Map([[user, rating]])
        invertedIndex.set(item, newItemIndex)
      }
    })
  })
  return invertedIndex
}

export const jaccardCoef = <T>(s1: Set<T>, s2: Set<T>): number => {
  return intersection(s1, s2).size / union(s1, s2).size
}

export const overlapCoef = <T>(s1: Set<T>, s2: Set<T>): number => {
  return intersection(s1, s2).size / Math.min(s1.size, s2.size)
}

export const adamicAdar = <T>(s1: Set<T>, s2: Set<T>, degreeIndex: Map<T, number>): number => {
  let total = 0
  intersection(s1, s2).forEach(t => {
    total += 1 / (Math.log2(degreeIndex.get(t) as number) + 1)
  })
  return total
}

export const resourceAllocation = <T>(s1: Set<T>, s2: Set<T>, degreeIndex: Map<T, number>): number => {
  let total = 0
  intersection(s1, s2).forEach(t => {
    total += 1 / (degreeIndex.get(t) as number)
  })
  return total
}

interface RatingStatement {
  username: UserID
  film_id: ItemID
  rating: string
  updated_at?: string
}
