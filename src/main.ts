import * as readline from 'readline'
import { evaluate, Evaluation, ExperimentalResults } from './evaluation'
import { readDatasets, apV, ReaderOpts, saveRecommendations, takeRelevantTestItems, sumValuesOfEntries } from './utils'

import { Ratings, RatingsIndex, TrustScores, UserID, UserWeight } from './types'
import * as sqlite3 from 'sqlite3'
import minimist from 'minimist'
import { SocialNetworkAwareRecommender, IBaseRecommender } from './recommenders'
import { createRecommender, RecommenderOpts } from './recommenders/recommender-factory'

sqlite3.verbose()
const argv = minimist(process.argv.slice(2), {
  boolean: ['interactive', 'use-trust-cache', 'save-results', 'cross-validation', 'use-defaults'],
  string: ['dataset', 'recommender', 'network-prefix', 'ensemble-recommenders', 'ensemble-weights', 'edge-weighting-strategy'],
  alias: {
    interactive: 'i',
    'use-trust-cache': 'c',
    recommender: 'r',
    'save-results': 's',
    dataset: 'd'
  },
  default: {
    dataset: 'letterboxd-50',
    'network-prefix': '',
    'm-neighbours': '150'
  }
})

const TOP_K_RECOMMENDED_ITEMS = 10
const MAX_TOP_K_RECOMMENDED_ITEMS = 100

const queryLoop = (rl: readline.ReadLine, recommender: IBaseRecommender, trainRatings: RatingsIndex, testRatings: RatingsIndex, trustScores: TrustScores, maxRating: number, db: sqlite3.Database) => async (query: string) => {
  const { evaluation } = await runEvaluation(recommender, query, trainRatings, testRatings, trustScores, maxRating, db)
  console.log(evaluation)
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  rl.question('query> ', queryLoop(rl, recommender, trainRatings, testRatings, trustScores, maxRating, db))
}

const runEvaluation = async (recommender: IBaseRecommender, user: UserID, trainRatings: RatingsIndex, testRatings: RatingsIndex, trustScores: TrustScores, maxRating: number, db: sqlite3.Database): Promise<ExperimentalResults> => {
  if (!trainRatings.has(user)) throw new UnknownUserError(`${user} is not in the training set`)
  if (!testRatings.has(user)) throw new UnknownUserError(`${user} is not in the test set`)
  const userTestRatings = testRatings.get(user) as Ratings
  const relevantItems = takeRelevantTestItems(trainRatings.get(user) as Ratings, userTestRatings, maxRating)
  if (relevantItems.size === 0) throw new EvaluationNotPossible('There are no relevant items of the user in the test set')
  const recommendations = await recommender.recommend(user, new Set(userTestRatings.keys()), MAX_TOP_K_RECOMMENDED_ITEMS)
  const evaluation = evaluate(recommendations, relevantItems, TOP_K_RECOMMENDED_ITEMS)

  const result: ExperimentalResults = {
    user,
    recommendations,
    relevantItems,
    testItems: new Set(userTestRatings.keys()),
    neighbours: new Array<UserID>(),
    evaluation
  }

  if (recommender instanceof SocialNetworkAwareRecommender) {
    const userTrustScores = recommender.getTrustScores(user)
    result.neighbours = userTrustScores.map(uw => uw.user)
    if (!trustScores.has(user)) saveTrustScores(db, user, recommender.name, userTrustScores)
  }

  return result
}

const saveTrustScores = (db: sqlite3.Database, source: UserID, recommenderName: string, trustScores: UserWeight[]): void => {
  if (trustScores.length === 0) return
  const topNNeighboursJSONString = JSON.stringify(trustScores)
  const sql = 'INSERT INTO trust_scores(user_id, recommender, trust_scores) VALUES(?, ?, ?) ' +
                  'ON CONFLICT(user_id, recommender) DO UPDATE SET trust_scores=excluded.trust_scores;'

  db.run(sql, [source, recommenderName, topNNeighboursJSONString])
}

const main = async (opts: minimist.ParsedArgs): Promise<void> => {
  if (opts['max-rating'] === undefined || opts.depth === undefined) throw new Error('Some of the arguments are missing (e.g. --max-rating, --depth)')
  const db = new sqlite3.Database('./db/trust-scores.db')
  const topMNeighbours: number = opts['m-neighbours']
  const numFolds = opts['cross-validation'] as boolean ? 5 : 1
  const edgeWeightingStrategy: string = opts['edge-weighting-strategy']
  const results: Evaluation[] = []
  for (let fold = 0; fold < numFolds; fold++) {
    const recOpts: RecommenderOpts = {
      depth: opts.depth,
      maxNNeighbours: topMNeighbours,
      edgeWeightingStrategy,
      useDefaults: opts['use-defaults']
    }
    if (opts.recommender === 'Ensemble') {
      recOpts.recommenders = opts['ensemble-recommenders'].split(',')
      recOpts.recommenderWeights = opts['ensemble-weights'].split(',').map((w: string) => parseInt(w))
    }
    const recommender = createRecommender(opts.recommender, recOpts)

    const datasetName: string = opts.dataset
    const networkPrefix: string = opts['network-prefix']
    const readerOpts: ReaderOpts = {
      train: `dataset/${datasetName}/ratings-folds/train-${fold}.jl`,
      test: `dataset/${datasetName}/ratings-folds/test-${fold}.jl`,
      socialNetwork: `dataset/${datasetName}/${datasetName}-${networkPrefix !== '' ? networkPrefix + '-' : ''}network.jl`,
      recommenderName: recommender.name,
      useTrustCache: opts['use-trust-cache']
    }

    const { ratings: { trainUserRatings, testRatings }, socialNetwork, trustScores } = await readDatasets(readerOpts)
    if (recommender instanceof SocialNetworkAwareRecommender) {
      recommender.fit({
        userRatings: trainUserRatings,
        socialNetwork: socialNetwork,
        trustScores
      })
    } else {
      recommender.fit({
        userRatings: trainUserRatings
      })
    }

    console.log('fold', fold)
    if (opts.interactive === true) {
      const rlQuery = readline.createInterface({
        input: process.stdin,
        output: process.stdout
      })
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      rlQuery.question('query> ', queryLoop(rlQuery, recommender, trainUserRatings, testRatings, trustScores, opts['max-rating'], db))
    } else {
      const experimentalResults = await Promise.allSettled(Array.from(testRatings.keys()).map(async (u) => await runEvaluation(recommender, u, trainUserRatings, testRatings, trustScores, opts['max-rating'], db)))
      const sensibleExperimentalResults = experimentalResults.filter(p => p.status === 'fulfilled').map(r => (r as PromiseFulfilledResult<ExperimentalResults>).value)
      const numUsers = sensibleExperimentalResults.length
      console.log('total', experimentalResults.length)
      console.log('predicted', numUsers)
      if (opts['save-results'] === true) {
        const recommenderName: string = opts.recommender === 'Appleseed' ? `Appleseed-${edgeWeightingStrategy}` : opts.recommender
        const filename = `${recommenderName}-${topMNeighbours}-${fold}-recommendations.csv`
        const withDefaultsPrefix = opts['use-defaults'] === true ? '-with-defaults' : ''
        await saveRecommendations(`evaluation/${datasetName}${withDefaultsPrefix}`, filename, sensibleExperimentalResults)
      }

      // hacky typing. don't know how to "type" it correct
      const result = Object.fromEntries(sensibleExperimentalResults.map(r => r.evaluation).map(Object.entries).reduce(sumValuesOfEntries).map(apV((v: number) => v / numUsers))) as unknown as Evaluation
      console.log(result)
      results.push(result)
    }
  }
  db.close()
  console.log('precision', results.reduce((totalPrecision, result) => totalPrecision + result.precision, 0) / results.length)
  console.log('ndcg', results.reduce((totalNdcg, result) => totalNdcg + result.ndcg, 0) / results.length)
}

class UnknownUserError extends Error {}
class EvaluationNotPossible extends Error {}

main(argv).catch(console.error)
