import { ItemID, ItemPrediction, RatingsIndex, SocialNetwork, UserID, UserWeight, WeightedItemPrediction, WeightedPrediction } from '../types'
import { createRecommender } from './recommender-factory'
import { ISocialNetworkAwareRecommender, ISocialNetworkAwareRecommenderData, ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class ParallelWeightedEnsembleRecommender extends SocialNetworkAwareRecommender {
  recommenders: ISocialNetworkAwareRecommender[]
  recommenderWeights: number[]

  constructor ({ depth, maxNNeighbours, recommenders, recommenderWeights, useDefaults, ...otherOpts }: IParallelWeightedEnsembleRecommenderOptions) {
    super({ name: 'ensemble', depth, maxNNeighbours, useDefaults })
    this.recommenders = recommenders.map(r => createRecommender(r, { depth, maxNNeighbours, ...otherOpts }) as SocialNetworkAwareRecommender)
    this.recommenderWeights = recommenderWeights
  }

  getTrustScores (user: UserID): UserWeight[] {
    return this.trustScores.get(user) ?? []
  }

  fit (data: ISocialNetworkAwareRecommenderData): void {
    this.recommenders.forEach(r => {
      r.fit(data)
    })
  }

  async getPredictions (user: UserID, candidateItems: Set<ItemID>): Promise<ItemPrediction[]> {
    const recommendersPredictions = await Promise.all(
      this.recommenders.map(async rec => await rec.getPredictions(user, candidateItems))
    )
    const predictionsWithWeight = recommendersPredictions.map((recPreds, recNo) => {
      return recPreds.map((pred): WeightedItemPrediction => {
        return {
          item: pred.item,
          prediction: pred.prediction * this.recommenderWeights[recNo],
          weight: this.recommenderWeights[recNo]
        }
      })
    })
    // this._mergePredictions.bind(this) ----> hard to spot error without the .bind(this)
    // if you only give this._mergePredictions as the function to reduce,
    // then it won't have the context bound to the "this" keyword
    // another option would have been this:
    // .reduce((preds1, preds2) => this._mergePredictions(preds1, preds2))
    const unsortedPredictions = predictionsWithWeight.reduce(this._mergePredictions.bind(this)).map((pred): ItemPrediction => ({ item: pred.item, prediction: pred.prediction / pred.weight }))
    return unsortedPredictions
  }

  _mergePredictions (preds1: WeightedItemPrediction[], preds2: WeightedItemPrediction[]): WeightedItemPrediction[] {
    const preds1Index: Map<ItemID, WeightedPrediction> = new Map(preds1.map(pred1 => [pred1.item, { weight: pred1.weight, prediction: pred1.prediction }]))
    const preds2Index: Map<ItemID, WeightedPrediction> = new Map(preds2.map(pred2 => [pred2.item, { weight: pred2.weight, prediction: pred2.prediction }]))

    preds1Index.forEach((pred1, item) => {
      if (preds2Index.has(item)) {
        preds2Index.set(item, this._sumWeightedPredictions(pred1, preds2Index.get(item) as WeightedPrediction))
      } else {
        preds2Index.set(item, pred1)
      }
    })

    return [...preds2Index].map(([item, pred]) => ({ item, prediction: pred.prediction, weight: pred.weight }))
  }

  _sumWeightedPredictions (pred1: WeightedPrediction, pred2: WeightedPrediction): WeightedPrediction {
    return {
      prediction: pred1.prediction * pred1.weight + pred2.prediction * pred2.weight,
      weight: pred1.weight + pred2.weight
    }
  }

  async computeTrustScores (user: UserID, localSocialNetwork: SocialNetwork, depth: number, userRatings?: RatingsIndex, itemRatings?: RatingsIndex): Promise<UserWeight[]> {
    return []
  }
}

export interface IParallelWeightedEnsembleRecommenderOptions extends Omit<ISocialNetworkAwareRecommenderOptions, 'name'> {
  recommenders: string[]
  recommenderWeights: number[]
}
