DROP TABLE IF EXISTS trust_scores;
CREATE TABLE trust_scores(user_id TEXT, recommender TEXT, trust_scores TEXT, PRIMARY KEY(user_id, recommender));
