import { UserID, SocialNetwork, UserWeight } from '../types'
import { getAllValues, intersection, union } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

// not easy in the 2.0 degree ego-network
// not all of the followers of the user2 are available
export class JaccardRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'jaccard'
    })
  }

  async computeTrustScores (user: UserID, { followees, followers }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const users = getAllValues(followees)
    users.delete(user)
    const trustScores: UserWeight[] = []
    users.forEach(user2 => {
      const commons = intersection(followees.get(user) as Set<UserID>, followers.get(user2) as Set<UserID>).size
      const all = union(followees.get(user) as Set<UserID>, followers.get(user2) as Set<UserID>).size
      trustScores.push({ user: user2, weight: commons / all })
    })

    return trustScores
  }
}
