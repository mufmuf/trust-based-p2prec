#!/usr/bin/env bash
for FILE in $1/ratings-folds/*.jl; do
    cat "$FILE" | cut -d'"' -f4,8,16 | sed 's/"/,/g' > ${FILE%.*}.csv
done
