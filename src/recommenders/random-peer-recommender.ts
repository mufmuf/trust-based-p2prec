import { UserID, SocialNetwork, UserWeight } from '../types'
import { getAllValues, shuffle } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class RandomPeerRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'uniform-trust'
    })
  }

  async computeTrustScores (user: UserID, { followees }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const trustScores: UserWeight[] = []
    const users = getAllValues(followees)
    users.delete(user)
    const usersList = [...users]
    shuffle(usersList)
    usersList.forEach(user2 => {
      trustScores.push({ user: user2, weight: 1 })
    })

    return trustScores
  }
}
