from surprise import SVD, KNNWithMeans
from surprise import Dataset
from surprise import Reader
from surprise.model_selection import PredefinedKFold
from operator import attrgetter, add, itemgetter
from functools import reduce
from evaluation import evaluate
from surprise.dump import dump, load
import argparse
from os import path

parser = argparse.ArgumentParser()
parser.add_argument('--use-dump', dest='use_dump', action='store_true')
parser.add_argument('--dataset')
parser.add_argument('--cross-validation', dest='cross_validation', action='store_true')
parser.add_argument('--max-rating', dest='max_rating')
parser.add_argument('--method')
args = parser.parse_args()

def sort_predictions(predictions):
    sorted_predictions_grouped_by_user = sorted(predictions, key=attrgetter('est'), reverse=True)
    return [pred.iid for pred in sorted_predictions_grouped_by_user]

def take_relevant_items(train_ratings, test_ratings, max_rating):
    all_ratings = train_ratings + test_ratings
    avg_rating = reduce(add, map(itemgetter(1), all_ratings)) / len(all_ratings)
    avg_rating = min(avg_rating, 0.75 * max_rating)
    return {item for (item, _) in filter(lambda x: x[1] >= avg_rating, test_ratings)}

def metrics_reducer(acc, curr):
    return {
        'precision': acc['precision'] + curr['precision'],
        'recall': acc['recall'] + curr['recall'],
        'f1': acc['f1'] + curr['f1'],
        'ndcg': acc['ndcg'] + curr['ndcg']
    }

init_metrics = {'precision': 0, 'recall': 0, 'f1': 0, 'ndcg': 0}

num_folds = 5 if args.cross_validation else 1
train_file_template = f'dataset/{args.dataset}/ratings-folds/train-{{}}.csv'
test_file_template = f'dataset/{args.dataset}/ratings-folds/test-{{}}.csv'
folds_files = [(train_file_template.format(i), test_file_template.format(i)) for i in range(num_folds)]

methods = {
    'svd': SVD,
    'knn': KNNWithMeans
}
method_args = {
    'svd': {
        'n_factors': 20,
    },
    'knn': {
        'k': 70,
        'sim_options': {
            'name': 'pearson',
            'user_based': True,
            'min_support': 10
        }
    }
}

dump_file = f'surprise/{args.method}.dump'
algo = methods.get(args.method)(**method_args.get(args.method))

# if not args.use_dump or not path.isfile(dump_file):
#   blabla
# else:
#     predictions, _ = load(dump_file)
data = Dataset.load_from_folds(folds_files, reader=Reader(sep=',', rating_scale=(1, int(args.max_rating))))
pkf = PredefinedKFold()

results_over_folds = []
for i, (trainset, testset) in enumerate(pkf.split(data)):
    train_file, test_file = folds_files[i]
    algo.fit(trainset)
    predictions = algo.test(testset)

    # dump(dump_file, predictions=predictions) -> make this separate for each fold
    train_ratings = {}
    with open(train_file) as f:
        for line in f:
            user, item, rating = line.strip().split(',')
            train_ratings.setdefault(user, [])
            train_ratings[user].append((item, int(rating)))

    test_ratings = {}
    with open(test_file) as f:
        for line in f:
            user, item, rating = line.strip().split(',')
            test_ratings.setdefault(user, [])
            test_ratings[user].append((item, int(rating)))

    predictions_grouped_by_user = {}
    for prediction in predictions:
        predictions_grouped_by_user.setdefault(prediction.uid, [])
        predictions_grouped_by_user[prediction.uid].append(prediction)

    sorted_predictions = {user: sort_predictions(preds) for user, preds in predictions_grouped_by_user.items()}

    evaluation_results = []
    for user, test in test_ratings.items():
        relevant_items = take_relevant_items(train_ratings[user], test, int(args.max_rating))
        if len(relevant_items) == 0:
            continue
        evaluation_results.append(evaluate(sorted_predictions[user], relevant_items, 20))

    print(len(evaluation_results))
    final_result = reduce(metrics_reducer, evaluation_results, init_metrics)

    final_result = {metric_name: metric_value / len(evaluation_results) for metric_name, metric_value in final_result.items()}
    results_over_folds.append(final_result)
    print(final_result)

result = reduce(metrics_reducer, results_over_folds, init_metrics)
result = {metric_name: metric_value / len(results_over_folds) for metric_name, metric_value in result.items()}
print('final result:')
print(result)
