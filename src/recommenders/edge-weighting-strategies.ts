import { UserID, RatingsIndex, Ratings } from '../types'
import { intersection, jaccardCoef, overlapCoef, computeCosineSimilarity } from '../utils'

const DEFAULT_VALUE = 0.01
export const commonNeighbours = (src: UserID, dst: UserID, userRatings: RatingsIndex): number => {
  if (!userRatings.has(src) || !userRatings.has(dst)) return DEFAULT_VALUE
  const srcItems = new Set((userRatings.get(src) as Ratings).keys())
  const dstItems = new Set((userRatings.get(dst) as Ratings).keys())
  return intersection(srcItems, dstItems).size
}

export const jaccard = (src: UserID, dst: UserID, userRatings: RatingsIndex): number => {
  if (!userRatings.has(src) || !userRatings.has(dst)) return DEFAULT_VALUE
  const srcItems = new Set((userRatings.get(src) as Ratings).keys())
  const dstItems = new Set((userRatings.get(dst) as Ratings).keys())
  return jaccardCoef(srcItems, dstItems)
}

export const overlap = (src: UserID, dst: UserID, userRatings: RatingsIndex): number => {
  if (!userRatings.has(src) || !userRatings.has(dst)) return DEFAULT_VALUE
  const srcItems = new Set((userRatings.get(src) as Ratings).keys())
  const dstItems = new Set((userRatings.get(dst) as Ratings).keys())
  return overlapCoef(srcItems, dstItems)
}

export const cosSim = (src: UserID, dst: UserID, userRatings: RatingsIndex): number => {
  if (!userRatings.has(src) || !userRatings.has(dst)) return DEFAULT_VALUE
  return computeCosineSimilarity(userRatings.get(src) as Ratings, userRatings.get(dst) as Ratings)
}
