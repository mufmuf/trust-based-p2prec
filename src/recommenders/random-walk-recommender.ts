import math from 'mathjs'
import { UserID, SocialNetwork, UserWeight } from '../types'
import { getAllValues } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class RandomWalkRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'random-walk'
    })
  }

  async computeTrustScores (user: UserID, { followees, followers }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const users = [...getAllValues(followees)]
    const trustScores: UserWeight[] = []
    const adjacencyMatrix: math.Matrix = math.zeros([users.length, users.length], 'sparse') as math.Matrix
    const usersInvertedIndex = new Map<UserID, number>()
    users.forEach((u, idx) => usersInvertedIndex.set(u, idx))
    users.forEach(user2 => {
      if (!followees.has(user2)) return
      const neighbourIndices = [...followees.get(user2) as Set<UserID>].filter(u => usersInvertedIndex.has(u)).map(u => usersInvertedIndex.get(u))
      // console.log(user2, neighbourIndices)
      // console.log(math.index(usersInvertedIndex.get(user2), neighbourIndices))
      // console.log(math.divide(math.ones(neighbourIndices.length), neighbourIndices.length))
      if (neighbourIndices.length === 0) return
      if (neighbourIndices.length === 1) {
        adjacencyMatrix.subset(math.index(usersInvertedIndex.get(user2), neighbourIndices), 1)
      } else {
        adjacencyMatrix.subset(math.index(usersInvertedIndex.get(user2), neighbourIndices), math.divide(math.ones(neighbourIndices.length), neighbourIndices.length))
      }
    })

    const m2 = math.multiply(adjacencyMatrix, adjacencyMatrix)

    math.flatten(m2.subset(math.index(0, math.range(0, m2.size()[1])))).toArray().forEach((v, idx) => {
      if (v === 0 || idx === 0) return // no trust score back to self please (idx === 0)
      trustScores.push({ user: users[idx], weight: v as number })
    })

    return trustScores
  }
}
