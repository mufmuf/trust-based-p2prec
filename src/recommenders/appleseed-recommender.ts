import Debug, { Debugger } from 'debug'
import { UserID, SocialNetwork, RatingsIndex, UserWeight, Ratings } from '../types'
import { computeCosineSimilarity, getAllValues } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

// import appleseed from '../@types/appleseed-metric'
// https://www.typescriptlang.org/docs/handbook/declaration-files/templates/module-function-d-ts.html
// couldn't make it to work somehow
import appleseed from 'appleseed-metric'
import { commonNeighbours, cosSim, jaccard, overlap } from './edge-weighting-strategies'

export class AppleseedRecommender extends SocialNetworkAwareRecommender {
  debug: Debugger
  initialEnergy: number
  decayRate: number
  threshold: number
  // how to make these function types more specific?
  edgeWeightingFn: Function
  edgeWeightingStrategies: { [index: string]: Function } = {
    CommonNeighbours: commonNeighbours,
    Jaccard: jaccard,
    Overlap: overlap,
    CosSim: cosSim,
    Unary: () => 1
  }

  constructor ({ edgeWeightingStrategy, ...opts }: AppleseedOptions) {
    super({
      ...opts,
      name: 'appleseed'
    })
    this.debug = Debug('appleseed-recommender')
    this.initialEnergy = 500
    this.decayRate = 0.85
    this.threshold = 0.3
    this.edgeWeightingFn = this.edgeWeightingStrategies[edgeWeightingStrategy]
  }

  async computeTrustScores (user: UserID, socialNetwork: SocialNetwork, depth: number, userRatings: RatingsIndex): Promise<UserWeight[]> {
    const nodes = new Set([user, ...getAllValues(socialNetwork.followees)])
    if (nodes.size === 1) return []

    const trustAssignments = this._socialNetworkToTrustAssignments(socialNetwork, nodes, userRatings)

    this.debug('trust assignments to consider: %d', trustAssignments.length)

    const result = await appleseed(user, trustAssignments, this.initialEnergy, this.decayRate, this.threshold)
    this.debug(`converged in ${result.iterations} iterations`)

    const trustScores = Object.entries(result.rankings as AppleseedRanking).sort((a, b) => b[1] - a[1])
    this.debug('depth-1 users', socialNetwork.followees.get(user))
    this.debug('trust scores', trustScores)
    return trustScores.map(([user2, score]) => {
      return { user: user2, weight: score }
      // if (userRatings.has(user) && userRatings.has(user2)) {
      //   const cosSim = computeCosineSimilarity(userRatings.get(user) as Ratings, userRatings.get(user2) as Ratings)
      //   return { user: user2, weight: (score * cosSim) / (score + cosSim) }
      // } else {
      //   return { user: user2, weight: 0.01 }
      // }
    })
  }

  _socialNetworkToTrustAssignments (socialNetwork: SocialNetwork, nodes: Set<UserID>, userRatings: RatingsIndex): TrustAssignment[] {
    const { followees } = socialNetwork
    const trustAssignments: TrustAssignment[] = []
    nodes.forEach(src => {
      if (!followees.has(src)) return
      (followees.get(src) as Set<UserID>).forEach(dst => {
        if (nodes.has(dst)) {
          const weight = this.edgeWeightingFn(src, dst, userRatings, socialNetwork)
          trustAssignments.push({ src, dst, weight: weight > 0 ? weight : 0.01 })
        }
      })
    })
    this.debug('trustAssignments', trustAssignments)
    return trustAssignments
  }

  // _followingIndexToTrustAssignmentsWithCosSim (followees: FollowIndex, nodes: Set<UserID>, ratings: RatingsIndex): TrustAssignment[] {
  //   const trustAssignments: TrustAssignment[] = []
  //   nodes.forEach(src => {
  //     if (!followees.has(src)) return
  //     (followees.get(src) as Set<UserID>).forEach(dst => {
  //       if (nodes.has(dst)) {
  //         if (!ratings.has(src) || !ratings.has(dst)) {
  //           trustAssignments.push({ src, dst, weight: 0.1 })
  //         } else {
  //           const cosSim = computeCosineSimilarity(ratings.get(src) as Ratings, ratings.get(dst) as Ratings)
  //           trustAssignments.push({ src, dst, weight: cosSim === 0 ? 0.1 : cosSim }) // to avoid having edges with 0 value
  //         }
  //       }
  //     })
  //   })
  //   this.debug('trustAssignments', trustAssignments)
  //   return trustAssignments
  // }
}

interface TrustAssignment {
  readonly src: UserID
  readonly dst: UserID
  readonly weight: number
}

interface AppleseedRanking {
  [index: string]: number
}

export interface AppleseedOptions extends Omit<ISocialNetworkAwareRecommenderOptions, 'name'>{
  edgeWeightingStrategy: string
}
