import { UserID, SocialNetwork, UserWeight } from '../types'
import { getAllValues, intersection } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class UniformInformationPropagationRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'uniform-ip'
    })
  }

  // Link Attachments by Estimating Probabilities of Information Propagation - Saito 2007
  async computeTrustScores (user: UserID, { followees, followers }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const users = getAllValues(followees)
    users.delete(user)
    const trustScores: UserWeight[] = []
    users.forEach(user2 => {
      trustScores.push({ user: user2, weight: this._informationPropagation(followees.get(user) as Set<UserID>, followers.get(user2) as Set<UserID>) })
    })

    return trustScores
  }

  _informationPropagation (users1: Set<UserID>, users2: Set<UserID>): number {
    const commonNeighbours = [...intersection(users1, users2)]
    const p = 0.5 // 0 < p < 1; it is not important what value p gets
    return 1 - Math.pow(p, 2 * commonNeighbours.length)
  }
}
