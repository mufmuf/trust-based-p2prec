import { BaseRecommender, IBaseRecommenderOptions } from './base-recommender'
import { ItemID, ItemPrediction } from '../types'
import { shuffle } from '../utils'

export class RandomRecommender extends BaseRecommender {
  async getPredictions (user: string, candidateItems: Set<string>): Promise<ItemPrediction[]> {
    throw new Error('Method not implemented.')
  }

  constructor (opts: Omit<IBaseRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'random'
    })
  }

  async recommend (user: string, candidateItems: Set<ItemID>, topK: number): Promise<ItemID[]> {
    const items = Array.from(candidateItems)
    shuffle(items)
    return items.slice(0, topK)
  }
}
