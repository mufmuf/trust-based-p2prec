import { sortRankings } from '../utils'
import { RatingsIndex, UserID, ItemID, ItemPrediction, Biases, Ratings } from '../types'
import quickselect from 'quickselect'

export abstract class BaseRecommender implements IBaseRecommender {
  userRatings: RatingsIndex = new Map()
  userBiases: Biases = new Map()
  globalMean: number = 0
  name: string
  useDefaults: boolean = false

  constructor ({ name, useDefaults }: IBaseRecommenderOptions) {
    this.name = name
    this.useDefaults = useDefaults
  }

  fit (opts: IBaseRecommenderData): void {
    this.userRatings = this._debiasRatings(opts.userRatings)
  }

  async recommend (user: UserID, candidateItems: Set<ItemID>, topK: number): Promise<ItemID[]> {
    const unsortedPredictions = await this.getPredictions(user, candidateItems)
    if (this.useDefaults) {
      const predictedItems = new Set(unsortedPredictions.map(p => p.item))
      const unpredictedItems = [...candidateItems].filter(i => !predictedItems.has(i))
      unpredictedItems.forEach(i => {
        unsortedPredictions.push({ item: i, prediction: 0 })
      })
    }
    return this.selectTopKRecommendedItems(unsortedPredictions, topK)
  }

  abstract getPredictions (user: UserID, candidateItems: Set<ItemID>): Promise<ItemPrediction[]>

  selectTopKRecommendedItems (unsortedPredictions: ItemPrediction[], topK: number): ItemID[] {
    if (topK < unsortedPredictions.length) {
      quickselect(unsortedPredictions, topK, 0, unsortedPredictions.length - 1, sortRankings)
    }
    return unsortedPredictions.slice(0, topK).sort(sortRankings).map(p => p.item)
  }

  _debiasRatings (userRatings: RatingsIndex): RatingsIndex {
    const debiasedUserRatings: Map<UserID, Ratings> = new Map()

    for (const [user, ratings] of userRatings) {
      const userTotal = [...ratings.values()].reduce((total, rating) => total + rating, 0)
      // TODO: check what shrink coef is good here
      // const userBias = userTotal / (userBiasShrinkCoef + ratings.size)
      const userBias = userTotal / ratings.size
      debiasedUserRatings.set(user, new Map([...ratings].map(([item, rating]) => [item, rating - userBias])))
    }

    return debiasedUserRatings
  }
}

export interface IBaseRecommender {
  name: string
  useDefaults: boolean
  fit (opts: IBaseRecommenderData): void
  recommend (user: UserID, candidateItems: Set<ItemID>, topK: number): Promise<readonly ItemID[]>
  getPredictions (user: UserID, candidateItems: Set<ItemID>): Promise<ItemPrediction[]>
  selectTopKRecommendedItems (unsortedPredictions: ItemPrediction[], topK: number): readonly ItemID[]
}

export interface IBaseRecommenderOptions {
  readonly name: string
  readonly useDefaults: boolean
}
export interface IBaseRecommenderData {
  readonly userRatings: RatingsIndex
}
