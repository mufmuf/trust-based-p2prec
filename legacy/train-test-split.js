const fs = require('fs')
const readline = require('readline')
const path = require('path')
const numCores = require('os').cpus().length
const { StaticPool } = require('node-worker-threads-pool')

if (require.main === module) {
  const argv = require('minimist')(process.argv.slice(2), {
    alias: {
      ratingsFile: 'r',
      networkFile: 'n',
      outputFolderName: 'o'
    }
  })

  main(argv)
}

function main ({ ratingsFile, networkFile, outputFolderName }) {
  const followees = {}
  const ratings = {}
  if (!ratingsFile || !networkFile || !outputFolderName) throw new Error('Ratings file, network file and output folder arguments are all required')
  readline.createInterface({
    input: fs.createReadStream(path.join(__dirname, networkFile)),
    output: process.stdout,
    terminal: false
  }).on('line', line => {
    const trustStatement = JSON.parse(line)
    const followeesOfUser = followees[trustStatement.username] || new Set()
    followeesOfUser.add(trustStatement.follows)
    if (!(trustStatement.username in followees)) {
      followees[trustStatement.username] = followeesOfUser
    }
  }).on('close', function () {
    readline.createInterface({
      input: fs.createReadStream(path.join(__dirname, ratingsFile)),
      output: process.stdout,
      terminal: false
    }).on('line', line => {
      const ratingStatement = JSON.parse(line)
      if (ratingStatement.rating === null) return
      const userRatings = ratings[ratingStatement.username] || {}
      userRatings[ratingStatement.film_id] = ratingStatement.rating
      if (!(ratingStatement.username in ratings)) {
        ratings[ratingStatement.username] = userRatings
      }
    }).on('close', async function () {
      const staticPool = new StaticPool({
        size: numCores - 2,
        workerData: {
          outputFolderName,
          ratings,
          followees
        },
        task: path.join(__dirname, 'split-write.js')
      })
      console.log('# users', Object.keys(followees).length)
      const responses = await Promise.all(
        Object.entries(followees).map(
          ([user, userFollowees], i) =>
            staticPool.exec({ user, userFollowees }).then(res => {
              console.log('done', i)
              return res
            })
        ))
      console.log('done all!')
      console.log('not have any ratings', responses.filter(res => res !== 0))
    })
  })
}
