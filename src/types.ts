export interface ItemPrediction {
  item: ItemID
  prediction: number
}

export interface WeightedItemPrediction extends ItemPrediction {
  weight: number
}

export type WeightedPrediction = Omit<WeightedItemPrediction, 'item'>

export interface UserWeight {
  readonly user: UserID
  readonly weight: number
}

export interface SocialNetwork {
  readonly followees: FollowIndex
  readonly followers: FollowIndex
}

export interface RatingsAll {
  readonly trainUserRatings: RatingsIndex
  readonly trainItemRatings: RatingsIndex
  readonly testRatings: RatingsIndex
}

export interface Dataset {
  readonly ratings: RatingsAll
  readonly socialNetwork: SocialNetwork
  readonly trustScores: TrustScores
}

export type Entry<T> = [string, T]
export type FollowIndex = Map<UserID, Set<UserID>>
export type UserID = string
export type ItemID = string
export type Ratings = Map<ItemID | UserID, number>
export type RatingsIndex = Map<UserID | ItemID, Ratings>
export type TrustScores = Map<UserID, UserWeight[]>
export type Biases = Map<UserID | ItemID, number>
