import { UserID, SocialNetwork, UserWeight } from '../types'
import { computeDistances } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class BFTrustRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'bf-trust'
    })
  }

  async computeTrustScores (user: UserID, { followees }: SocialNetwork, depth: number): Promise<UserWeight[]> {
    const distanceToTrustScore = (distance: number): number => {
      // maybe we can try alternative distance metrics
      // return 1 / Math.pow(distance, 3)
      // return 1 - (distance - 1) / depth
      return 1 / distance
    }

    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const distances = computeDistances(user, followees, depth)
    const trustScores: UserWeight[] = []
    distances.forEach((distance, user2) => {
      trustScores.push({ user: user2, weight: distanceToTrustScore(distance) })
    })

    return trustScores
  }
}
