declare module 'quickselect' {
  export default function quickselect<T> (arr: T[], k: number, left?: number, right?: number, compareFn?: ((comparable1: T, comparable2: T) => number)): void
}
