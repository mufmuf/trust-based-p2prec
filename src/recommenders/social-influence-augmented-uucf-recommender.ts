import { UserID, SocialNetwork, RatingsIndex, UserWeight, Ratings } from '../types'
import { computeCosineSimilarity, getAllValues } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class SocialInfluenceAugmentedUUCFRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'social-influence'
    })
  }

  async computeTrustScores (user: UserID, { followees, followers }: SocialNetwork, depth: number, ratings: RatingsIndex): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const trustScores: UserWeight[] = []
    if (!ratings.has(user)) return trustScores
    const users = getAllValues(followees)
    users.delete(user)
    users.forEach(user2 => {
      if (!ratings.has(user2)) return
      const sim = computeCosineSimilarity(ratings.get(user) as Ratings, ratings.get(user2) as Ratings)
      const inB = (followers.get(user2) as Set<UserID>).size
      const outA = (followees.get(user) as Set<UserID>).size
      const influenceValue = Math.sqrt(inB / (outA + inB)) // Zhou et al. - SocialFM
      trustScores.push({ user: user2, weight: sim * influenceValue })
    })

    return trustScores
  }
}
