# Takes the scraped dataset from mubi/letterboxd 
# and splits the data to train and test sets
import sys
import argparse
import json

def main(rating_file, network_file):
    with open(rating_file) as f:
        ratings = [json.loads(line) for line in f]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--rating_file', required=True, help='ratings file in jsonlines format - required fields [username, film_id, rating]')
    parser.add_argument('-n', '--network_file', required=True, help='network file in jsonlines format - required fields [username, follows]')
    args = parser.parse_args()
    main(args.rating_file, args.network_file)