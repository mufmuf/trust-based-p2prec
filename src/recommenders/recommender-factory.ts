import { AdamicAdarRecommender, AppleseedRecommender, BFTrustRecommender, CommonNeighboursRecommender, EgonRecommender, GlobalTopPopularRecommender, IBaseRecommender, ParallelWeightedEnsembleRecommender, LocalTopPopularRecommender, LocalUserBasedCollaborativeFiltering, RandomRecommender, RandomWalkRecommender, ResourceAllocationRecommender, SocialInfluenceAugmentedUUCFRecommender, UniformInformationPropagationRecommender, RandomPeerRecommender } from '../recommenders'
import { AppleseedOptions } from './appleseed-recommender'
import { IBaseRecommenderOptions } from './base-recommender'
import { JaccardRecommender } from './jaccard-recommender'
import { IParallelWeightedEnsembleRecommenderOptions } from './parallel-weighted-ensemble-recommender'
import { ISocialNetworkAwareRecommenderOptions } from './social-network-aware-recommender'

export const createRecommender = (recommenderType: string, opts: RecommenderOpts): IBaseRecommender => {
  // if you move this out of the function
  // Ensemble Constructor becomes undefined!
  // maybe it's related to the order of loading modules and/or scoping, but how?
  const recommenders: RecommenderConstructorsCollection = {
    // Global baselines
    Random: RandomRecommender,
    GlobalTopPopular: GlobalTopPopularRecommender,

    // Local baselines
    RandomPeer: RandomPeerRecommender,
    LocalTopPopular: LocalTopPopularRecommender,
    BFTrust: BFTrustRecommender,
    LocalUserBasedCF: LocalUserBasedCollaborativeFiltering,

    // Link Prediction
    CommonNeighbours: CommonNeighboursRecommender,
    Jaccard: JaccardRecommender,
    AdamicAdar: AdamicAdarRecommender,
    ResourceAllocation: ResourceAllocationRecommender,
    UniformInformationPropagation: UniformInformationPropagationRecommender,

    // Trust Propagation
    Appleseed: AppleseedRecommender,

    // Hybrid
    Ensemble: ParallelWeightedEnsembleRecommender,

    SocialInfluenceAugmentedUUCF: SocialInfluenceAugmentedUUCFRecommender,
    RandomWalk: RandomWalkRecommender,
    Egon: EgonRecommender
  }
  const RecommenderConstructor = recommenders[recommenderType]
  return new RecommenderConstructor(opts)
}

// the following does not work because the constructor of BaseRecommender may have
// a different type signature than the constructor of the classes that extend it
// interface RecommendersCollection {
//   [index: string]: typeof BaseRecommender
// }
interface RecommenderConstructorsCollection {
  [index: string]: new (...args: any[]) => IBaseRecommender
}

export type RecommenderOpts = Partial<IBaseRecommenderOptions & ISocialNetworkAwareRecommenderOptions & IParallelWeightedEnsembleRecommenderOptions & AppleseedOptions>
