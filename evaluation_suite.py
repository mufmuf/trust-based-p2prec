import subprocess

node = '/home/ege/.nvm/versions/node/v14.16.0/bin/node'
edge_weighting_strategies = ['Unary', 'CommonNeighbours', 'Jaccard', 'Overlap', 'CosSim']
ensemble_recs_list = ["'Appleseed,LocalUserBasedCF'"]
ensemble_weights_list = ["'3,1'"]
datasets = ['letterboxd', 'mubi']
num_core_users = '200'
m_neighbours_list = ['20', '100', '350', '1000']

def run(custom_args):
    command_template="{node} -r source-map-support/register dist/main.js -r {rec} \
--edge-weighting-strategy {edge_weighting_strategy} \
--ensemble-recommenders {ensemble_recs} \
--ensemble-weights {ensemble_weights} \
--depth 2 --dataset {dataset} \
--max-rating {max_rating} \
--m-neighbours {m_neighbours} \
-s --cross-validation"
    dummy_args = {
        'edge_weighting_strategy': 'dummy',
        'ensemble_recs': 'dummy',
        'ensemble_weights': 'dummy',
        'm_neighbours': 0
    }
    command = command_template.format(**{**dummy_args, **custom_args})
    
    # hacky
    # when output is redirected, somehow print did not actually print the command (i think it prints it the last)
    subprocess.call(['echo', f"'{command}'"]) 
    
    subprocess.call(command.split(' '))

# for dataset in datasets:
#     for rec in ['Random', 'GlobalTopPopular', 'LocalTopPopular']:
#         custom_args = {
#             'node': node,
#             'dataset': f'{dataset}-{num_core_users}',
#             'max_rating': 10 if dataset == 'letterboxd' else 5,
#             'rec': rec,
#         }
#         run(custom_args)

# # Neighbourhood based approaches
# for dataset in datasets:
#     for rec in ['RandomPeer', 'BFTrust', 'LocalUserBasedCF', 'CommonNeighbours', 'Jaccard', 'AdamicAdar', 'ResourceAllocation', 'UniformInformationPropagation', 'Appleseed']:
#         for m_neighbours in m_neighbours_list:
#             custom_args = {
#                 'node': node,
#                 'dataset': f'{dataset}-{num_core_users}',
#                 'max_rating': 10 if dataset == 'letterboxd' else 5,
#                 'rec': rec,
#                 'm_neighbours': m_neighbours
#             }
#             if rec == 'Appleseed':
#                 for edge_weighting_strategy in edge_weighting_strategies:
#                     custom_args = {
#                         **custom_args,
#                         'edge_weighting_strategy': edge_weighting_strategy
#                     }
#                     run(custom_args)
#             else:
#                 run(custom_args)

# ensemble approaches
for dataset in datasets:
    for m_neighbours in m_neighbours_list:
        custom_args = {
            'node': node,
            'dataset': f'{dataset}-{num_core_users}',
            'max_rating': 10 if dataset == 'letterboxd' else 5,
            'rec': 'Ensemble',
            'ensemble_recs': 'Appleseed,LocalUserBasedCF',
            'edge_weighting_strategy': 'CommonNeighbours',
            'ensemble_weights': '2,1',
            'm_neighbours': int(m_neighbours) // 2
        }
        run(custom_args)
