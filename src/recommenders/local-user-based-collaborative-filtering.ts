import { UserID, SocialNetwork, RatingsIndex, UserWeight, Ratings } from '../types'
import { computeCosineSimilarity, getAllValues } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class LocalUserBasedCollaborativeFiltering extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'local-ubcf'
    })
  }

  async computeTrustScores (user: UserID, { followees }: SocialNetwork, depth: number, ratings: RatingsIndex): Promise<UserWeight[]> {
    if (depth < 1) throw new Error('depth cannot be lower than 1')
    const trustScores: UserWeight[] = []
    if (!ratings.has(user)) return trustScores
    const users = getAllValues(followees)
    users.delete(user)
    users.forEach(user2 => {
      if (!ratings.has(user2)) return
      const sim = computeCosineSimilarity(ratings.get(user) as Ratings, ratings.get(user2) as Ratings)
      trustScores.push({ user: user2, weight: sim })
    })

    return trustScores
  }
}
