// Provides a strategy to calculate recommendations based on the
// declared neighbourhood of the user

import { UserWeight, RatingsIndex, ItemPrediction, ItemID, Ratings } from './types'
import { apV } from './utils'

export class Neighbourhood {
  interpolate (neighbours: UserWeight[], neighbourRatings: RatingsIndex, candidateItems: Set<ItemID>): ItemPrediction[] {
    const ratingAccumulator: Map<ItemID, NeighbourRatingAccumulator> = new Map()
    for (const { user: neighbour, weight } of neighbours) {
      // may be better to include only the top N neighbours who have rated items.
      // Here we have less than N neighbours if some of them have not rated any items.
      if (!neighbourRatings.has(neighbour)) continue // if the user has not rated any items.
      for (const [item, rating] of (neighbourRatings.get(neighbour) as Ratings)) {
        if (!candidateItems.has(item)) continue
        const acc = ratingAccumulator.get(item) ?? { ratingSum: 0, weightSum: 0, numNeighbours: 0 }
        acc.ratingSum += weight * rating
        acc.weightSum += weight
        acc.numNeighbours += 1
        if (!ratingAccumulator.has(item)) ratingAccumulator.set(item, acc)
      }
    }
    return Array.from(ratingAccumulator).map(apV(computeRating))
      .map(item => ({ item: item[0], prediction: item[1] }))
  }
}

// alternatively a shrink coefficient may be applied to punish neighbourhoods without much data
// one needs to find such coefficient by cross validation
// const computeRating = (neighbourRating: NeighbourRatingAccumulator): number => {
//   return neighbourRating.ratingSum / (shrinkCoef + neighbourRating.weightSum)
// }

const computeRating = (neighbourRating: NeighbourRatingAccumulator): number => {
  const dampingFactor = 1 / (1 + Math.exp(-neighbourRating.numNeighbours)) // logistic/sigmoid function
  return dampingFactor * neighbourRating.ratingSum / neighbourRating.weightSum
}

interface NeighbourRatingAccumulator {
  ratingSum: number
  weightSum: number
  numNeighbours: number
}
