import { adamicAdar, intersection, jaccardCoef, overlapCoef, resourceAllocation } from './utils'
import appleseed from 'appleseed-metric'

const followees = [
  new Set([1, 2, 3]),
  new Set([0, 4, 5, 6]),
  new Set([1, 3, 6]),
  new Set([0, 1, 5, 6, 7, 8]),
  new Set([]),
  new Set([]),
  new Set([]),
  new Set([]),
  new Set([])
]

const followers = [
  new Set([1, 3]),
  new Set([0, 2, 3]),
  new Set([0]),
  new Set([0, 2]),
  new Set([1]),
  new Set([1, 3]),
  new Set([1, 2, 3]),
  new Set([3]),
  new Set([3])
]

const trustAssignments = [
  { src: '0', dst: '1', weight: 0.7 },
  { src: '0', dst: '2', weight: 0.5 },
  { src: '0', dst: '3', weight: 0.9 },
  { src: '1', dst: '0', weight: 0.7 },
  { src: '1', dst: '4', weight: 0.6 },
  { src: '1', dst: '5', weight: 0.2 },
  { src: '1', dst: '6', weight: 0.9 },
  { src: '2', dst: '1', weight: 0.6 },
  { src: '2', dst: '3', weight: 0.3 },
  { src: '2', dst: '6', weight: 0.5 },
  { src: '3', dst: '0', weight: 0.9 },
  { src: '3', dst: '1', weight: 0.8 },
  { src: '3', dst: '5', weight: 0.4 },
  { src: '3', dst: '6', weight: 0.8 },
  { src: '3', dst: '7', weight: 0.3 },
  { src: '3', dst: '8', weight: 0.6 }
]

const degreeIndex = new Map(followers.map((s, i) => [i, s.size]))

for (let i = 1; i < 9; i++) {
  const results = [
    intersection(followees[0], followers[i]).size,
    jaccardCoef(followees[0], followers[i]),
    overlapCoef(followees[0], followers[i]),
    adamicAdar(followees[0], followers[i], degreeIndex),
    resourceAllocation(followees[0], followers[i], degreeIndex)
  ]
  console.log(['cn', 'jacc', 'overl', 'aa', 'ra'].join('\t'), i)
  console.log(results.map(r => r.toLocaleString(undefined,
    { minimumFractionDigits: 2 })).join('\t'))
  console.log()
}

appleseed('0  ', trustAssignments, 100, 0.85, 0.5).then((result, _) => {
  console.log(`converged in ${result.iterations} iterations`)
  console.log('rankings', result.rankings)
})
