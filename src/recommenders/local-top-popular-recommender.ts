import { UserID, ItemID, Ratings, UserWeight, ItemPrediction } from '../types'
import { computeDistances } from '../utils'
import { ISocialNetworkAwareRecommenderOptions, SocialNetworkAwareRecommender } from './social-network-aware-recommender'

export class LocalTopPopularRecommender extends SocialNetworkAwareRecommender {
  constructor (opts: Omit<ISocialNetworkAwareRecommenderOptions, 'name'>) {
    super({
      ...opts,
      name: 'local-top-popular'
    })
  }

  async getPredictions (user: UserID, candidateItems: Set<ItemID>): Promise<ItemPrediction[]> {
    const distances = computeDistances(user, this.socialNetwork.followees, this.depth).keys()
    const recommendedItems: Map<ItemID, number> = new Map()
    for (const neighbour of distances) {
      if (!(this.userRatings.has(neighbour))) continue // if the user has not rated any items.
      for (const [item] of (this.userRatings.get(neighbour) as Ratings)) {
        if (!candidateItems.has(item)) continue
        const popularity = recommendedItems.get(item) ?? 0
        recommendedItems.set(item, popularity + 1)
      }
    }
    const unsortedPredictions = Array.from(recommendedItems)
      .map(item => ({ item: item[0], prediction: item[1] }))
    return unsortedPredictions
  }

  async computeTrustScores (): Promise<UserWeight[]> {
    return []
  }
}
