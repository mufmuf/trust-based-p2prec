const fs = require('fs')
const path = require('path')
const { parentPort, workerData } = require('worker_threads')

parentPort.on('message', ({ user, userFollowees }) => {
  parentPort.postMessage(splitAndWrite({ ...workerData, user, userFollowees }))
})

const splitAndWrite = ({ user, userFollowees, ratings, followees, outputFolderName }) => {
  const depth2followees = [...userFollowees].filter(e => e in followees).map(e => followees[e]).reduce(union, userFollowees)
  depth2followees.delete(user) // user is excluded from depth2followees.
  const depth2followeesRatedMovies = [...depth2followees].map(f => ratings[f]).filter(r => r !== undefined).map(r => new Set(Object.keys(r))).reduce(union, new Set())
  if (!(user in ratings)) return user
  const folds = KFoldSplit(ratings[user], depth2followeesRatedMovies, 5)
  for (let i = 0; i < folds.length; i++) {
    const [train, test] = folds[i]
    const trainRows = Object.keys(train).map(movie => [user, movie, ratings[user][movie]].join('\t')).join('\n')
    const testRows = Object.entries(test).map(([movie, relevance]) => [user, movie, +relevance].join('\t')).join('\n') // converts true to 1 and false to 0
    fs.appendFile(path.join(__dirname, outputFolderName, `train-${i}.csv`), trainRows + '\n', () => { })
    fs.appendFile(path.join(__dirname, outputFolderName, `test-${i}.csv`), testRows + '\n', () => { })
  }
  return 0
}

const union = (setA, setB) => {
  if (setB === undefined) return setA
  const _union = new Set(setA)
  for (const elem of setB) {
    _union.add(elem)
  }
  return _union
}

const KFoldSplit = (userRatings, neighbourMovies, k = 5) => {
  const averageRating = Object.values(userRatings).reduce((a, b) => a + b) / Object.keys(userRatings).length
  const train = {}
  const candidateTest = {}
  for (const [movie, rating] of Object.entries(userRatings)) {
    if (neighbourMovies.has(movie)) candidateTest[movie] = (rating >= averageRating)
    else train[movie] = rating // the movies that were not rated by any of the neighbours of the user
  }
  const candidateTestMovies = Object.keys(candidateTest)
  shuffle(candidateTestMovies)

  const testFolds = Array.from(Array(k).keys(), _ => [])
  for (let i = 0; i < candidateTestMovies.length; i++) {
    testFolds[i % k].push(candidateTestMovies[i])
  }

  const splits = []
  for (let i = 0; i < k; i++) {
    const trainMovies = joinFoldsExcept(testFolds, i)
    const testMovies = testFolds[i]
    Object.keys(train).forEach(movie => { trainMovies.push(movie) })
    const tr = Object.fromEntries(trainMovies.map(m => [m, userRatings[m]]))
    const te = Object.fromEntries(testMovies.map(m => [m, candidateTest[m]]))
    splits.push([tr, te])
  }

  return splits
}

const joinFoldsExcept = (testFolds, i) => {
  return testFolds.filter((_, idx) => idx !== i).reduce((arr1, arr2) => arr1.concat(arr2), [])
}
const shuffle = array => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
    [array[i], array[j]] = [array[j], array[i]]
  }
}
