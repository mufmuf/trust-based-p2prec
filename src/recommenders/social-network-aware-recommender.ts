import { BaseRecommender, IBaseRecommender, IBaseRecommenderData, IBaseRecommenderOptions } from './base-recommender'
import { Neighbourhood } from '../neighbourhood'
import { TrustScores, UserID, UserWeight, ItemID, RatingsIndex, Ratings, Biases, SocialNetwork, ItemPrediction } from '../types'
import { computeDistances, getAllValues, invertRatingsIndex, takeKeyOfEntry, takeMapSubset } from '../utils'

export abstract class SocialNetworkAwareRecommender extends BaseRecommender implements ISocialNetworkAwareRecommender {
  trustScores: TrustScores = new Map()
  neighbourhood: Neighbourhood = new Neighbourhood()
  socialNetwork: SocialNetwork = { followees: new Map(), followers: new Map() }
  maxNNeighbours: number
  depth: number

  constructor ({ depth, maxNNeighbours, ...rest }: ISocialNetworkAwareRecommenderOptions) {
    super(rest)
    this.depth = depth
    this.maxNNeighbours = maxNNeighbours
  }

  getTrustScores (user: UserID): UserWeight[] {
    return this.trustScores.get(user) ?? []
  }

  fit ({ userRatings, socialNetwork, trustScores }: ISocialNetworkAwareRecommenderData): void {
    this.userRatings = userRatings
    this.socialNetwork = socialNetwork
    this.trustScores = trustScores
  }

  _addBiases (ratings: Ratings, biases: Biases): Ratings {
    return new Map([...ratings].map(([item, rating]) => [item, rating + (biases.get(item) as number)]))
  }

  _getLocalSocialNetwork (user: UserID, socialNetwork: SocialNetwork, depth: number): SocialNetwork {
    const distances = computeDistances(user, socialNetwork.followees, depth)
    const users = [user, ...[...distances].filter(([_, d]) => d < depth).map(takeKeyOfEntry)]
    return {
      followees: takeMapSubset(socialNetwork.followees, users),
      followers: takeMapSubset(socialNetwork.followers, [user, ...distances.keys()])
    }
  }

  async getPredictions (user: UserID, candidateItems: Set<ItemID>): Promise<ItemPrediction[]> {
    const localSocialNetwork = this._getLocalSocialNetwork(user, this.socialNetwork, this.depth)
    const localUsers = getAllValues(localSocialNetwork.followees)
    localUsers.add(user)
    const localUserRatings = takeMapSubset(this.userRatings, [...localUsers])
    const localItemRatings = invertRatingsIndex(localUserRatings)
    const debiasedUserRatings = this._debiasRatings(localUserRatings)
    const neighbours = await this.computeTrustScores(user, localSocialNetwork, this.depth, localUserRatings, localItemRatings)
    this.trustScores.set(user, neighbours)

    const topNNeighboursWeights = neighbours.sort((a, b) => b.weight - a.weight).slice(0, this.maxNNeighbours)
    const neighbourRatings: RatingsIndex = new Map()
    topNNeighboursWeights.forEach(({ user }) => {
      if (localUserRatings.has(user)) {
        neighbourRatings.set(user, debiasedUserRatings.get(user) as Ratings)
      }
    })

    const unsortedPredictions = this.neighbourhood.interpolate(topNNeighboursWeights, neighbourRatings, candidateItems)
    return unsortedPredictions
  }

  abstract computeTrustScores (user: UserID, localSocialNetwork: SocialNetwork, depth: number, userRatings?: RatingsIndex, itemRatings?: RatingsIndex): Promise<UserWeight[]>
}

export interface ISocialNetworkAwareRecommender extends IBaseRecommender {
  fit (opts: ISocialNetworkAwareRecommenderData): void
  getTrustScores (user: UserID): readonly UserWeight[]
  computeTrustScores (user: UserID, socialNetwork: SocialNetwork, depth: number, userRatings?: RatingsIndex, itemRatings?: RatingsIndex): Promise<readonly UserWeight[]>
}

export interface ISocialNetworkAwareRecommenderData extends IBaseRecommenderData {
  readonly socialNetwork: SocialNetwork
  trustScores: TrustScores
}

export interface ISocialNetworkAwareRecommenderOptions extends IBaseRecommenderOptions {
  depth: number
  maxNNeighbours: number
}
