export = appleseed
declare function appleseed (source: string, trustAssignments: Appleseed.TrustAssignment[], initialEnergy: number, spreadingFactor: number, threshold: number): Promise<Appleseed.Result>

declare namespace Appleseed {
  export interface TrustAssignment {
    readonly src: string
    readonly dst: string
    readonly weight: number
  }

  export interface Result {
    rankings: {}
    graph: any
    iterations: number
  }
}
